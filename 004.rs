fn main() {
	println!("{:?}", get_largest_palindrome(3));
}

fn get_largest_palindrome(digit:i32) -> i32 {
	let mut largest = 0;
	let mut max = 1;

	for n in 0..digit {
		max = max * 10;
	}

	let min = max / 10;
	max = max - 1;

	for i in (min..max).rev() {
		for j in (min..max).rev() {
			if is_palindrome(i * j) {
				// return i * j;

				largest = std::cmp::max(largest, i * j);
				println!("{:?} x {:?}", i, j);
			}
		}
	}

	return largest;
	// return -1;
}

fn is_palindrome(n:i32) -> bool {
	let n_s = n.to_string();
	let limit = n_s.len() / 2;

	return 	n_s.bytes().take(limit).eq(
			n_s.bytes().rev().take(limit)
	);
}