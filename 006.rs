fn main() {
	// println!("{:?}", get_sum_square(100));
	// println!("{:?}", get_square_sum(100));
	println!("{:?}", (get_square_sum(100) - get_sum_square(100)));
}

fn get_sum_square(n:i64) -> i64 {
	let mut sum = 0;
	
	for i in 1..(n + 1) {
		// println!("{:?}: {:?}", i, sum);
		sum = sum + (i * i);
	}

	return sum;
}

fn get_square_sum(n:i64) -> i64 {
	let mut sum = 0;

	for i in 1..(n + 1) {
		// println!("{:?}: {:?}", i, sum);
		sum = sum + i;
	}

	return sum * sum;
}