fn main() {
	println!("{:?}", get_sum_of_multiples(1000, &[3, 5]));
}

fn get_sum_of_multiples(max:i32, multiples: &[i32]) -> i32 {
	let mut sum:i32 = 0;
	let mut i:i32 = 0;
	let mut is_multiple:bool = false;

	while i < max {
		for multiple in multiples.iter() {
			if (i % multiple) == 0 {
				is_multiple = true;
			}
		}

		if is_multiple {
			sum = sum + i;
			is_multiple = false;
		}

		i = i + 1;
	}

	return sum;
}