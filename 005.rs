fn main() {
	let mut i = 1;
	loop {
		if is_divisible_by_all_between(i, 1, 20) {
			println!("{:?}", i);
			break;
		}
		i = i + 1;
	}
}

fn is_divisible_by_all_between(n:i32, min:i32, max:i32) -> bool {
	for i in min..max {
		if n % i != 0 {
			return false;
		}
	}

	return true;
}