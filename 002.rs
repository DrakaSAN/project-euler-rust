fn main() {
	println!("{:?}", sum_fibonacci_up_to(1, 2, 0, 4000000));	
}

fn sum_fibonacci_up_to(a:i32, b:i32, sum:i32, max:i32) -> i32 {
	if b < max {
		if (b % 2) == 0 {
			return sum_fibonacci_up_to(b, b + a, sum + b, max);
		} else {
			return sum_fibonacci_up_to(b, b + a, sum, max);
		}
	} else {
		return sum;
	}
}