fn main() {
	let max = get_max_prime_factor_of(600851475143);

	println!("{:?}", max);
}

fn get_max_prime_factor_of(n:i64) -> i64 {
	let root = (n as f64).sqrt();
	let mut max_factor = 0;

	for i in 1..(root.trunc() as i64) {
		if is_prime(i) {
			if n % i == 0 {
				max_factor = i;
			}
		}
	}

	return max_factor;
}

fn is_prime(n:i64) -> bool {
	let root = (n as f64).sqrt();

	for i in 2..(root.trunc() as i64) {
		if n % i == 0 {
			//println!("i: {:?}", i);
			//println!("is_prime({:?}) -> {:?}", n, false);
			return false;
		}
	}

	//println!("is_prime({:?}) -> {:?}", n, true);
	return true;
}