// fn main() {
// 	println!("{:?}", is_prime(4)); 
// }

fn main() {
	let mut prime_numbers = 1;
	let mut i = 2;

	while prime_numbers < 10002 {
		if is_prime(i) {
			println!("{:?}: {:?}", prime_numbers, i);
			prime_numbers = prime_numbers + 1;
		}
		i = i + 1;
	}
	// println!("{:?}: {:?}", prime_numbers, 1);
}

fn is_prime(n:i64) -> bool {
	let root = (n as f64).sqrt().ceil() as i64;

	// println!("n: {:?}\troot: {:?}", n, root);

	//Edge case, 2 is prime, but also it's own root
	if n == 2 {
		// println!("is_prime({:?}) -> {:?}", n, true);
		return true;
	}

	for i in 2..root {
		if n % i == 0 {
			// println!("i: {:?}", i);
			// println!("is_prime({:?}) -> {:?} ({:?})", n, false, i);
			return false;
		}
	}

	if n % root == 0 {
		// println!("i: {:?}", i);
		// println!("is_prime({:?}) -> {:?} ({:?})", n, false, root);
		return false;
	}

	// println!("is_prime({:?}) -> {:?}", n, true);
	return true;
}